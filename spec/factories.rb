FactoryGirl.define do
  factory :user do
    name     "Rik Megens"
    email    "rik@example.com"
    password "foobar"
    password_confirmation "foobar"
  end
end
