require 'spec_helper'

describe "User pages" do

  let(:base_title) { "Ruby on Rails Tutorial Sample App" }

	describe "signup page" do

		before { visit signup_path }

    let(:submit) { "Create my account" }

		it "should have the h1 'Signup'" do
      page.should have_selector('h1', :text => 'Sign up')
    end

    it "should have the base title" do
      page.should have_selector('title', :text => "#{base_title} | Sign up")
    end

    describe "met ongeldige informatie" do
      it "zal geen nieuwe gebruiker aan mogen maken" do
        expect { click_button submit }.not_to change(User, :count)
      end
    end

    describe "met geldige informatie" do
      before do
        fill_in "Name",             with: "Aad Demo"
        fill_in "Email",            with: "Aad.Demo@mail.weg"
        fill_in "Password",         with: "AadDemo"
        fill_in "Confirmation",     with: "AadDemo"
      end

      it "zal een nieuwe gebruiker aanmaken" do
        expect { click_button submit }.to change(User, :count).by(1)
      end
    end
  end

  describe "profile page" do
    let(:user) { FactoryGirl.create(:user) }
    before { visit user_path(user) }

    it "should have the name in h1" do
      page.should have_selector('h1',    text: user.name)
    end
    it "should have the name in the title" do
      page.should have_selector('title',  text: user.name)
    end
  end
  
end
